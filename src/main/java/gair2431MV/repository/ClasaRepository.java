package gair2431MV.repository;

import java.util.HashMap;
import java.util.List;

import gair2431MV.utils.ClasaException;

import gair2431MV.model.Corigent;
import gair2431MV.model.Elev;
import gair2431MV.model.Medie;
import gair2431MV.model.Nota;

public interface ClasaRepository {

	public void creazaClasa(List<Elev> elevi, List<Nota> note);
	public HashMap<Elev, HashMap<String, List<Double>>> getClasa();
	public List<Medie> calculeazaMedii() throws ClasaException;
	public void afiseazaClasa();
	public List<Corigent> getCorigenti() throws ClasaException;
}
