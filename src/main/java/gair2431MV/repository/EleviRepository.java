package gair2431MV.repository;


import java.util.List;

import gair2431MV.model.Elev;

public interface EleviRepository {
	public void addElev(Elev e);
	public List<Elev> getElevi();
	public void readElevi(String fisier);
}
