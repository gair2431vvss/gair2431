package gair2431MV.repository;

import java.util.List;

import gair2431MV.utils.ClasaException;

import gair2431MV.model.Nota;

public interface NoteRepository {

	public void addNota(Nota nota) throws ClasaException;
	public List<Nota> getNote();
	public void readNote(String fisier);

}
