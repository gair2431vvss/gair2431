package gair2431MV.controller;

import java.util.HashMap;
import java.util.List;

import gair2431MV.model.Corigent;
import gair2431MV.model.Elev;
import gair2431MV.model.Medie;
import gair2431MV.model.Nota;
import gair2431MV.repository.ClasaRepository;
import gair2431MV.repository.ClasaRepositoryMock;
import gair2431MV.repository.EleviRepository;
import gair2431MV.repository.EleviRepositoryMock;
import gair2431MV.repository.NoteRepository;
import gair2431MV.repository.NoteRepositoryMock;
import gair2431MV.utils.ClasaException;

public class NoteController {
	private NoteRepository note;
	private ClasaRepository clasa;
	private EleviRepository elevi;

	public NoteController() {
		note = new NoteRepositoryMock();
		clasa = new ClasaRepositoryMock();
		elevi = new EleviRepositoryMock();
	}

	public NoteController(NoteRepository note, ClasaRepository clasa, EleviRepository elevi) {
		this.note = note;
		this.clasa = clasa;
		this.elevi = elevi;
	}
	public void addNota(Nota nota) throws ClasaException {
		note.addNota(nota);

	}

	public void addElev(Elev elev) {
		elevi.addElev(elev);
	}

	public void creeazaClasa(List<Elev> elevi, List<Nota> note) {
		clasa.creazaClasa(elevi, note);
	}

	public List<Medie> calculeazaMedii() throws ClasaException {
		return clasa.calculeazaMedii();

	}

	public List<Nota> getNote() {
		return note.getNote();
	}

	public List<Elev> getElevi() {
		return elevi.getElevi();
	}

	public HashMap<Elev, HashMap<String, List<Double>>> getClasa() {
		return clasa.getClasa();
	}

	public void afiseazaClasa() {
		clasa.afiseazaClasa();
	}

	public void readElevi(String fisier) {
		elevi.readElevi(fisier);
	}

	public void readNote(String fisier) {
		note.readNote(fisier);
	}

	public List<Corigent> getCorigenti() throws ClasaException {
		return clasa.getCorigenti();
	}
}
