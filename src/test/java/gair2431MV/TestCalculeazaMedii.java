package gair2431MV;

import gair2431MV.controller.NoteController;
import gair2431MV.model.Elev;
import gair2431MV.model.Medie;
import gair2431MV.model.Nota;
import gair2431MV.utils.ClasaException;
import gair2431MV.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static java.lang.Float.NaN;
import static org.junit.Assert.assertEquals;


public class TestCalculeazaMedii {
    private NoteController ctrl;

    @Before
    public void init(){
        ctrl = new NoteController();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void test1() throws ClasaException {
        Elev e1 = new Elev(1, "Elev1");
        Elev e2 = new Elev(2, "Elev2");
        ctrl.addElev(e1);
        ctrl.addElev(e2);
        Nota n1 = new Nota(1,"Materie1", 10);
        Nota n2 = new Nota(1,"Materie1", 7);
        Nota n3 = new Nota(1,"Materie2", 10);
        Nota n4 = new Nota(1,"Materie2", 10);
        Nota n5 = new Nota(2,"Materie2", 4);
        Nota n6 = new Nota(2,"Materie2", 3);
        Nota n7 = new Nota(2,"Materie2", 6);
        Nota n8 = new Nota(2,"Materie1", 7);
        ctrl.addNota(n1);
        ctrl.addNota(n2);
        ctrl.addNota(n3);
        ctrl.addNota(n4);
        ctrl.addNota(n5);
        ctrl.addNota(n6);
        ctrl.addNota(n7);
        ctrl.addNota(n8);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(rezultate.size(),2);
    }

    @Test
    public void test2() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.emptyRepository);
        ctrl.calculeazaMedii();
    }

    @Test
    public void test3() throws ClasaException {
        Elev e1 = new Elev(1, "Anca");
        Elev e2 = new Elev(2, "Violeta");
        ctrl.addElev(e1);
        ctrl.addElev(e2);
        Nota n1 = new Nota(1,"Materie1", 10);
        ctrl.addNota(n1);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        for(Medie m : rezultate){
            if(m.getElev().getNrmatricol() == 2)
                assertEquals(m.getMedie(),NaN,0.0001);
            if(m.getElev().getNrmatricol() == 1)
                assertEquals(m.getMedie(),10,0.0001);
        }
    }

    @Test
    public void test4() throws ClasaException {
        Elev e1 = new Elev(1, "Mihaiela");
        Elev e2 = new Elev(2, "Georgeta");
        ctrl.addElev(e1);
        ctrl.addElev(e2);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        for(Medie m : rezultate)
            if(m.getElev().getNrmatricol() == 1)
                assertEquals(m.getMedie(),NaN,0.0001);
        for(Medie m1 : rezultate)
            if(m1.getElev().getNrmatricol() == 2)
                assertEquals(m1.getMedie(),NaN,0.0001);
    }

    @Test
    public void test5() throws ClasaException{
        Elev e1 = new Elev(1, "Alexandra");
        Elev e2 = new Elev(2, "Valentina");
        Elev e3 = new Elev(3, "Catalina");
        ctrl.addElev(e1);
        ctrl.addElev(e2);
        ctrl.addElev(e3);
        Nota n1 = new Nota(1,"Algebra", 10);
        Nota n2 = new Nota(2,"Algebra", 7);
        Nota n3 = new Nota(2,"Fizica", 10);

        ctrl.addNota(n1);
        ctrl.addNota(n2);
        ctrl.addNota(n3);

        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        assertEquals(rezultate.size(),3);

        for(Medie m : rezultate)
            if(m.getElev().getNrmatricol() == 1)
                assertEquals(m.getMedie(), 10.0,0.0001);
            else
            if(m.getElev().getNrmatricol() == 2)
                assertEquals(m.getMedie(), 8.5,0.0001);
            else
            if(m.getElev().getNrmatricol() == 1)
                assertEquals(m.getMedie(), NaN,0.0001);
    }

    @Test
    public void test6() throws ClasaException {
        Elev e1 = new Elev(1, "Anca");
        Elev e2 = new Elev(2, "Violeta");
        ctrl.addElev(e1);
        ctrl.addElev(e2);
        Nota n1 = new Nota(1,"", 10);
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        List<Medie> rezultate = ctrl.calculeazaMedii();
        for(Medie m : rezultate)
            if(m.getElev().getNrmatricol() == 1)
                assertEquals(m.getMedie(),NaN,0.0001);
        for(Medie m1 : rezultate)
            if(m1.getElev().getNrmatricol() == 2)
                assertEquals(m1.getMedie(),NaN,0.0001);
    }
}
