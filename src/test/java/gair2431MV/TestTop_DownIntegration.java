package gair2431MV;
import gair2431MV.controller.NoteController;
import gair2431MV.model.Corigent;
import gair2431MV.model.Elev;
import gair2431MV.model.Medie;
import gair2431MV.model.Nota;
import gair2431MV.utils.ClasaException;
import gair2431MV.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestTop_DownIntegration {
    private NoteController ctrl;

    @Before
    public void setUp() throws Exception {
        ctrl = new NoteController();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testA() throws ClasaException {
        //testare unitara pentru modulul A (A-valid)
        Nota nota1 = new Nota(2, "Analiza Matematica", 1);
        Nota nota2 = new Nota(2,"Franceza", 3);
        Nota nota3 = new Nota(2,"Franceza", 4);
        ctrl.addNota(nota1);
        ctrl.addNota(nota2);
        ctrl.addNota(nota3);
        assertEquals(3, ctrl.getNote().size());
        assertEquals(ctrl.getNote().get(2).getNrmatricol(), 2);
        assertEquals(ctrl.getNote().get(2).getMaterie(), "Franceza");
        assertEquals(ctrl.getNote().get(2).getNota(), 4, 0.001);
    }

    @Test
    public void testBAInt() throws ClasaException{
        //testare de integrare a modulului B
        //P->B->A A-valid B-invalid
        //B
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.emptyRepository);
        ctrl.calculeazaMedii();
        //A
        Nota nota1 = new Nota(1000, "Desena", 10);
        ctrl.addNota(nota1);
        assertEquals(ctrl.getNote().size(),1);
    }

    @Test
    public void testCombinate() throws ClasaException{
        //testare de integrare a modulului C;
        //P->B->A->C B-invalid A-valid C-valid
        //B
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.emptyRepository);
        List<Medie> rezultate = ctrl.calculeazaMedii();
        //A
        Elev e1 = new Elev(1, "Elev1");
        ctrl.addElev(e1);
        Nota nota = new Nota(1, "Desena", 4);
        ctrl.addNota(nota);
        assertEquals(1, ctrl.getNote().size());
        ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
        //C
        List<Corigent> corigenti1 = ctrl.getCorigenti();
        assertEquals(corigenti1.size(),1);
    }
}
