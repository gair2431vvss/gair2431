package gair2431MV.model;

import gair2431MV.controller.NoteController;
import gair2431MV.utils.ClasaException;
import gair2431MV.utils.Constants;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class NotaTest {
    private NoteController ctrl;

    @Before
    public void init() {
        ctrl = new NoteController();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    //ecp valide

    @Test
    public void test1() throws ClasaException {
        Nota nota = new Nota(1, "Desen", 10);
        ctrl.addNota(nota);
        assertEquals(1, ctrl.getNote().size());
    }

    @Test
    public void test2() throws ClasaException {
        //expectedEx.expect(ClasaException.class);
        //expectedEx.expectMessage(Constants.invalidNrmatricol);
        Nota nota = new Nota(10, "Istorie", 1);
        ctrl.addNota(nota);
    }

    //ecp nonvalide

    @Test
    public void test3() throws ClasaException {
        // expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNrmatricol);
        Nota nota = new Nota(655555, "Istorie", 3);
        ctrl.addNota(nota);
    }

//    @Test
//    public void test4() throws ClasaException {
//        Nota nota = new Nota(1, "Desen", "dasnjd");
//        ctrl.addNota(nota);
//        assertEquals(1, ctrl.getNote().size());
//    }

    //bva
 //   @Test
//    public void test5() throws ClasaException {
//        //expectedEx.expect(ClasaException.class);
//        //expectedEx.expectMessage(Constants.invalidNrmatricol);
//        Nota nota = new Nota(-1, "Istorie", 1);
//        ctrl.addNota(nota);
//    }
//
//    @Test
//    public void test6() throws ClasaException {
//        // expectedEx.expect(ClasaException.class);
//        //expectedEx.expectMessage(Constants.invalidNrmatricol);
//        Nota nota = new Nota(0, "Istorie", 3);
//        ctrl.addNota(nota);
//    }
    @Test
    public void test7() throws ClasaException {
        Nota nota = new Nota(1, "Desen", 10);
        ctrl.addNota(nota);
        assertEquals(1, ctrl.getNote().size());
    }

//    @Test
//    public void test8() throws ClasaException {
//        //expectedEx.expect(ClasaException.class);
//        //expectedEx.expectMessage(Constants.invalidNrmatricol);
//        Nota nota = new Nota(78, "Isto", 1);
//        ctrl.addNota(nota);
//    }

    @Test
    public void test9() throws ClasaException {
        // expectedEx.expect(ClasaException.class);
        //expectedEx.expectMessage(Constants.invalidNrmatricol);
        Nota nota = new Nota(2, "Desen", 3);
        ctrl.addNota(nota);
    }

    @Test
    public void test10() throws ClasaException {
        // expectedEx.expect(ClasaException.class);
        //expectedEx.expectMessage(Constants.invalidNrmatricol);
        Nota nota = new Nota(8, "Desenu", 3);
        ctrl.addNota(nota);
    }
//    @Test
//    public void test11() throws ClasaException {
//        // expectedEx.expect(ClasaException.class);
//        //expectedEx.expectMessage(Constants.invalidNrmatricol);
//        Nota nota = new Nota(8, "Desenu", 0);
//        ctrl.addNota(nota);
//    }
    @Test
    public void test12() throws ClasaException {
        // expectedEx.expect(ClasaException.class);
        //expectedEx.expectMessage(Constants.invalidNrmatricol);
        Nota nota = new Nota(8, "Desenu", 1);
        ctrl.addNota(nota);
    }
    @Test
    public void test13() throws ClasaException {
        // expectedEx.expect(ClasaException.class);
        //expectedEx.expectMessage(Constants.invalidNrmatricol);
        Nota nota = new Nota(8, "Desenu", 8);
        ctrl.addNota(nota);
    }
}